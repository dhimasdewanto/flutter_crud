class Buku {
  final int id;
  final String judul;
  final String kategori;
  final double harga;

  Buku({this.id, this.judul, this.kategori, this.harga});

  Buku.fromMap(Map<String, dynamic> mapData) :
    id = mapData['id'],
    judul = mapData['judul'],
    kategori = mapData['kategori'],
    harga = mapData['harga'];

  Map<String, dynamic> toMap() => {
    'judul': judul,
    'kategori': kategori,
    'harga': harga,
  };
}