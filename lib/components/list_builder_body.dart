import 'package:flutter/material.dart';
import 'package:flutter_crud/detail_buku.dart';
import 'package:flutter_crud/models/buku.dart';
import 'package:transparent_image/transparent_image.dart';

class ListBuilderBody extends StatelessWidget {
  final _imageUrl = "https://pendidikan.lentera.co.id/wp-content/uploads/2019/01/Ilustrasi-Buku.jpg";
  final Buku buku;

  ListBuilderBody(this.buku);

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      padding: EdgeInsets.zero,
      onPressed: () {
        Navigator.push(context, MaterialPageRoute(
          builder: (context) => DetailBuku(buku)
        ));
      },
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                Container(
                  height: 20.0,
                  width: 20.0,
                  child: CircularProgressIndicator(
                    strokeWidth: 2.0,
                  ),
                ),
                Container(
                  height: 80.0,
                  child: Hero(
                    tag: "${buku.id}",
                    child: FadeInImage.memoryNetwork(
                      placeholder: kTransparentImage,
                      image: _imageUrl,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            flex: 7,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 10.0),
              child:_ListBuilderBodyData(buku)
            ),
          ),
        ],
      )
    );
  }
}

class _ListBuilderBodyData extends StatelessWidget {
  final Buku buku;

  _ListBuilderBodyData(this.buku);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text("${buku.judul}", 
          style: TextStyle(
            fontSize: 20.0,
            fontWeight: FontWeight.bold
          )
        ),
        SizedBox(height: 5.0),
        Text("${buku.kategori}"),
      ],
    );
  }
}