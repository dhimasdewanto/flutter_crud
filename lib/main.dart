import 'package:flutter/material.dart';
import 'package:flutter_crud/components/list_builder_body.dart';
import 'package:flutter_crud/models/buku.dart';
import 'package:flutter_crud/services/buku_service.dart';
import 'package:flutter_crud/tambah_buku.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.dark(),
      title: "Flutter Crud",
      home: IndexMenu(),
    );
  }
}

class IndexMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Flutter Crud"),
      ),
      body: _GetBuku(),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        heroTag: "tambah",
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(
            builder: (BuildContext context) => TambahBuku())
          );
        },
      ),
    );
  }
}

class _GetBuku extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _GetBukuState();
}

class _GetBukuState extends State<_GetBuku> {
  var listBuku = List<Buku>();
  var isLoading = true;
  final bukuService = BukuService();

  @override
  void initState() {
    super.initState();
    _loading();
  }

  @override
  Widget build(BuildContext context) {
    if(isLoading) {
      return Center(
        child: CircularProgressIndicator()
      );
    }

    return RefreshIndicator(
      onRefresh: _loading,
      child: ListView.builder(
        itemCount: listBuku.length,
        itemBuilder: (BuildContext context, int index) {
          final buku = listBuku[index];
          return Dismissible(
            key: Key(buku.id.toString()),
            direction: DismissDirection.endToStart,
            onDismissed: (DismissDirection direction) {
              // Remove buku on database
              bukuService.deleteBuku(buku.id);
              setState(() {
               listBuku.remove(buku); 
              });
              Scaffold.of(context).showSnackBar(
                SnackBar(
                  content: Text("${buku.judul} telah dihapus")
                )
              );
            },
            child: Column(children: <Widget>[
              ListBuilderBody(buku),
              Divider(height: 0),
            ]),
          );
        },
      )
    );
  }

  Future<void> _loading() async {
    final listBuku = await bukuService.getAllBuku();
    setState(() {
      this.listBuku = listBuku;
      isLoading = false;
    });
    print("Success Loading");
  }
}