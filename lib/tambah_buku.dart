import 'package:flutter/material.dart';
import 'package:flutter_crud/main.dart';
import 'package:flutter_crud/models/buku.dart';
import 'package:flutter_crud/services/buku_service.dart';

class TambahBuku extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Tambah Buku"),
      ),
      body: _TambahBukuForm(),
    );
  }
}

class _TambahBukuForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _TambahBukuFormState();
  }
}

class _TambahBukuFormState extends State<_TambahBukuForm> {
  final bukuService = BukuService();
  var controllerJudul = TextEditingController();
  var controllerKategori = TextEditingController();
  var controllerHarga = TextEditingController();
  
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 50.0),
      alignment: Alignment.center,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text("Tambah Buku",
            style: TextStyle(
              fontSize: 25.0
            ),
          ),
          SizedBox(height: 10.0),
          TextFormField(
            controller: controllerJudul,
            decoration: InputDecoration(
              labelText: "Judul"
            ),
          ),
          SizedBox(height: 10.0),
          TextFormField(
            controller: controllerKategori,
            decoration: InputDecoration(
              labelText: "Kategori"
            ),
          ),
          SizedBox(height: 10.0),
          TextFormField(
            controller: controllerHarga,
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
              labelText: "Harga"
            ),
          ),
          SizedBox(height: 20.0),
          FloatingActionButton.extended(
            icon: Icon(Icons.add),
            label: Text("Tambah"),
            heroTag: "tambah",
            onPressed: () {
              _addBook(context);
            },
          ),
        ],
      ),
    );
  }

  Future<void> _addBook(context) async {
    final judul = controllerJudul.value.text;
    final kategori = controllerKategori.value.text;
    final harga = double.parse(controllerHarga.value.text);

    final buku = Buku(
      judul: judul,
      kategori: kategori,
      harga: harga,
    );

    final responseBool = await bukuService.postBuku(buku);

    if (responseBool) {
      Navigator.push(context, MaterialPageRoute(
        builder: (BuildContext context) => IndexMenu())
      );
    }
  }
}