import 'package:flutter/material.dart';
import 'package:flutter_crud/models/buku.dart';

class DetailBuku extends StatelessWidget {
  final _imageUrl = "https://pendidikan.lentera.co.id/wp-content/uploads/2019/01/Ilustrasi-Buku.jpg";

  final Buku buku;
  DetailBuku(this.buku);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(buku.judul),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Hero(
            tag: "${buku.id}",
            child: Image.network(_imageUrl),
          ),
          Padding(
            padding: EdgeInsets.all(20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("${buku.judul}", 
                  style: TextStyle(
                    fontSize: 30.0,
                    fontWeight: FontWeight.bold
                  )
                ),
                SizedBox(height: 10.0),
                Text("${buku.kategori}"),
                SizedBox(height: 10.0),
                Text("Rp. ${buku.harga.round()}", 
                  style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold
                  )
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}