import 'dart:convert';

import 'package:flutter_crud/models/buku.dart';
import 'package:http/http.dart' as http;

class BukuService {
  final baseUrl = "http://dev.accelist.com:1203/api/exam/";
  final headers = {
    'Content-type' : 'application/json', 
    'Accept': 'application/json',
  };

  Future<List<Buku>> getAllBuku() async {
    final response = await http.get("${baseUrl}get-book");

    if (response.statusCode != 200) {
      return List<Buku>();
    }

    return _jsonToListBuku(response.body);
  }

  Future<bool> postBuku(Buku bukuBaru) async {
    final response = await http.post(
      "${baseUrl}post-book/",
      headers: headers,
      body: jsonEncode(bukuBaru.toMap())
    );

    if(response.statusCode >= 200 && response.statusCode <= 299) {
      return true;
    }
    
    return false;
  }

  Future<bool> deleteBuku(int bukuId) async {
    final response = await http.delete("${baseUrl}delete-book/$bukuId");

    if(response.statusCode >= 200 && response.statusCode <= 299) {
      return true;
    }
    
    return false;
  }

  List<Buku> _jsonToListBuku(String jsonString) {
    final listData = json.decode(jsonString);
    return List<Buku>.from(listData.map((item) => Buku.fromMap(item)));
  }
}
